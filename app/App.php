<?php

namespace App;

use App\like\LikeHelper;

class App
{
    /** @var array $config */
    protected $config;

    /** @var App $instance */
    public static $instance;

    public function __construct(array $config)
    {
        if (!is_null(self::$instance))
        {
            throw new \Exception('Application already exists!');
        }
        $this->config = $config;
        self::$instance = $this;
    }

    public function run()
    {
        $helper = new LikeHelper();
        $helper->like(1969867297061485562);
    }

    public function config()
    {
        return $this->config;
    }
}