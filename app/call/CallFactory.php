<?php

namespace App\Call;

use App\App;
use App\Auth\AuthHelper;

class CallFactory
{
    public static function getTokenCall()
    {
        return new Call("https://www.instagram.com");
    }

    public static function getAuthCall()
    {
        return new Call(
            "https://www.instagram.com/accounts/login/ajax/",
            true,
            [
                'username' => App::$instance->config()['auth']['login'],
                'password' => App::$instance->config()['auth']['password']
            ],
            [
                "x-csrftoken: " . AuthHelper::getCSRFToken(),
                "x-requested-with:XMLHttpRequest"
            ]
        );
    }

    public static function getLikeCall(int $photoId)
    {
        return new SecuredCall(
            "https://www.instagram.com/web/likes/$photoId/like/",
            true,
            [],
            [
                'x-csrftoken: ' . AuthHelper::getCSRFToken(),
                'x-requested-with: XMLHttpRequest'
            ]
        );
    }
}