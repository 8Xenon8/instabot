<?php


namespace App\Call;


use App\Auth\AuthHelper;

class SecuredCall extends Call
{
    protected function getHeaders()
    {
        if (!AuthHelper::isAuthenticated())
        {
            AuthHelper::authenticate();
        }
        $headers = parent::getHeaders();
        $headers[] = "x-csrftoken: " . AuthHelper::getCSRFToken();

        return $headers;
    }
}