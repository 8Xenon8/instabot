<?php

namespace App\Call;

use App\Auth\AuthHelper;

class Call
{
    protected static $curl;

    protected $url;
    protected $isPost;
    protected $postParams;
    protected $headers;

    protected function getHeaders()
    {
        return [
            'x-requested-with: XMLHttpRequest'
        ];
    }

    public function __construct(string $url, bool $isPost = false, array $postParams = [], array $headers = [])
    {
        $this->url = $url;
        $this->isPost = $isPost;
        $this->postParams = $postParams;
        $this->headers = $headers;
    }

//    protected abstract function processResult();

    /** @TODO: make method protected */
    public function getCurl()
    {
        if (!self::$curl)
        {
            self::$curl = curl_init();
            curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER,1);
            curl_setopt(self::$curl, CURLOPT_HEADER, 1);
            curl_setopt(self::$curl, CURLOPT_COOKIEJAR, DATA_DIR . '/cookies/cookie.txt');
        }

        return self::$curl;
    }

    public function commit()
    {
        $ch = self::getCurl();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_POST, $this->isPost);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->postParams));
        curl_setopt($ch, CURLOPT_URL, $this->url);

        $output = curl_exec($ch);

        $match = [];
        preg_match('/csrftoken\=[A-Za-z0-9]{32}/', $output, $match);

        $token = str_replace("csrftoken=", '', $match[0]);
        AuthHelper::$csrftoken = $token;

        return $output;
    }
}