<?php
/**
 * Created by PhpStorm.
 * User: xenon
 * Date: 9/16/2019
 * Time: 6:45 PM
 */

namespace App\like;

use App\Auth\AuthHelper;
use App\Call\CallFactory;

class LikeHelper
{
    public function like(int $photoId)
    {
        AuthHelper::authenticate();

        $likeCall = CallFactory::getLikeCall($photoId);
        print_r($likeCall->commit());
    }

    public static function unlike(int $photoId)
    {

    }
}