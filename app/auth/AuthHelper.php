<?php


namespace App\Auth;


use App\Call\CallFactory;

class AuthHelper
{
    protected static $authData;

    public static $csrftoken;

    public static function authenticate()
    {
        $call = CallFactory::getAuthCall();
        /** @TODO: process auth data */
        $data = $call->commit();

        $status = strpos($data, '{"status": "ok"}') !== false;
        self::$authData = $status;
    }

    public static function isAuthenticated()
    {
        /** @TODO: refactor this stub */
        return self::$authData == true;
    }

//    public static function getAuthData()
//    {
//        if (!self::$authData || empty(self::$authData))
//        {
//            self::authenticate();
//        }
//
//        return self::$authData;
//    }

    public static function getCSRFToken()
    {
        if (!self::$csrftoken)
        {
            /** @TODO: refactor */
            $call = CallFactory::getTokenCall();
            $data = $call->commit();
            $match = [];
            preg_match('/csrftoken\=[A-Za-z0-9]{32}/', $data, $match);

            self::$csrftoken = str_replace("csrftoken=", '', $match[0]);
        }
        return self::$csrftoken;
    }

    protected static function parseAuthData($data)
    {
//        $headers = substr($data, 0, $this->headersLength - 1);
//        $content = substr($data, $this->headersLength);

//        $authData = [];
//
//        foreach (explode("\n", $headers) as $header)
//        {
//            if (strpos($header, 'Set-Cookie') !== false)
//            {
//                $authData[] = $header;
//            }
//        }
//
//        return $authData;
    }
}