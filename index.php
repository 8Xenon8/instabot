<?php

require_once __DIR__ . '/vendor/autoload.php';

$config = require_once __DIR__ . '/config/config.php';

define('APP_DIR', __DIR__);
define('DATA_DIR', APP_DIR . '/data');

$app = new \App\App($config);
$app->run();